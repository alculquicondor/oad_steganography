#include "decoder.h"

Decoder::Decoder() : BaseProcessor() {
}

File * Decoder::operator () (const string &imgfn, const string &filefn) {
    steganographier->decode(imgfn, filefn);
}

