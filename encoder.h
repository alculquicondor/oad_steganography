#ifndef ENCODER_H
#define ENCODER_H
#include "baseprocessor.h"

class Encoder : public BaseProcessor {
    public:
        Encoder();
        virtual File * operator() (const string &imgfn, const string &filefn);
};

#endif
