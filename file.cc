#include "file.h"

File::File() {
    data = nullptr;
    _size = 0;
}

File::~File() {
    if (data != nullptr)
        delete[] data;
}

void File::save(string filename) const {
    std::ofstream file(filename + "." + extension, std::ios::binary | std::ios::out);
    file.write((char *)data, _size);
}

string File::get_extension() const {
    return extension;
}

size_t File::size() const {
    return _size;
}

