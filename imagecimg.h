#ifndef IMAGECIMG_H
#define IMAGECIMG_H
#include "file.h"
#include <CImg.h>

using namespace cimg_library;
typedef CImg<unsigned char> Img;

class ImageCimg : public File {
    private:
        typedef unsigned char uchar;
        Img *img;
    public:
        ImageCimg();
        virtual ~ImageCimg();
        virtual void save(string filename) const;
        friend class SteganographierCimg;
};


#endif
