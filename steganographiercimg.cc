#include "steganographiercimg.h"
#ifdef DEBUG
#include <cassert>
#include <iostream>
using namespace std;
#endif

ImageCimg *SteganographierCimg::encode(const string &imgfn, const string &filefn) {
    Img *src;
    try {
        src = new Img(imgfn.c_str());
    } catch (std::exception e) {
        return nullptr;
    }
    std::ifstream file(filefn, std::ios::in | std::ios::binary | std::ios::ate);
    if (file.is_open()) {
        size_t fsize = file.tellg();
        if ((fsize<<2)+28 > src->size()) {
            file.close();
            return nullptr;
        }
        byte *data = new byte[fsize+7];
        strcpy((char *)data, filefn.substr(filefn.rfind('.')+1, 3).c_str());
#ifdef DEBUG
        cout << src->size() << endl;
        cout << fsize << endl;
        cout << data << endl;
#endif
        int bpos = 24;
        size_t idx = 3;
        while (bpos >= 0) {
            data[idx++] = (fsize & (0xff << bpos)) >> bpos;
            bpos -= 8;
        }
#ifdef DEBUG
        cout << hex << (int)data[3] << " " << (int)data[4] << " " << (int)data[5]
            << " " << (int)data[6] << dec << endl;
#endif
        file.seekg(0, std::ios::beg);
        file.read((char*)data+7, fsize);
        file.close();
        bpos = 6;
        idx = 0;
        for (auto it = src->begin(); it != src->end() and idx < fsize+7; ++it) {
            *it &= ~3u;
            *it += (data[idx] & (3 << bpos)) >> bpos;
            bpos -= 2;
            if (bpos < 0) {
                bpos = 6;
                ++idx;
            }
        }
        ImageCimg *cod = new ImageCimg;
        cod->img = src;
        cod->extension = "png";
        delete []data;
        return cod;
    }
    return nullptr;
}

File *SteganographierCimg::decode(const string &imgfn, const string &filefn) {
    Img *src;
    try {
        src = new Img(imgfn.c_str());
    } catch (std::exception e) {
        return nullptr;
    }
    File *dec = new File;
    byte ext[4];
    memset(ext, 0, sizeof ext);
    int bpos = 6;
    size_t idx = 0;
    auto it = src->begin();
    for (int i = 0; i < 12; ++i, ++it) {
        ext[idx] += (*it & 3) << bpos;
        bpos -= 2;
        if (bpos < 0) {
            bpos = 6;
            ++idx;
        }
    }
    dec->extension.assign((char *)ext, 3);
    for (int i = 0; i < 16; ++i, ++it)
        dec->_size = (dec->_size<<2) + (*it & 3);
    dec->data = new byte[dec->_size];
    memset(dec->data, 0, dec->_size);
    idx = 0;
    bpos = 6;
    while (idx < dec->_size) {
        dec->data[idx] += (*it & 3) << bpos;
        bpos -= 2;
        if (bpos < 0) {
            bpos = 6;
            ++idx;
        }
        ++it;
    }
    if (filefn != "")
        dec->save(filefn);
    return dec;
}

