#ifndef DECODER_H
#define DECODER_H
#include "baseprocessor.h"

class Decoder : public BaseProcessor {
    public:
        Decoder();
        virtual File * operator() (const string &imgfn, const string &filefn = "");
};

#endif
