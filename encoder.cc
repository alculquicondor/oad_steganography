#include "encoder.h"

Encoder::Encoder() : BaseProcessor() {
}

File * Encoder::operator () (const string &imgfn, const string &filefn) {
    steganographier->encode(imgfn, filefn);
}

