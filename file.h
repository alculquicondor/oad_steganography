#ifndef FILE_H
#define FILE_H
#include <string>
#include <fstream>
using std::string;
typedef unsigned char byte;

class File {
    protected:
        string extension;
        size_t _size;
        byte *data;
    public:
        File();
        virtual ~File();
        virtual void save(string filename) const;
        string get_extension() const;
        size_t size() const;
        friend class SteganographierCimg;
};

#endif
