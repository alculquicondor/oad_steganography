#ifndef STEGANOGRAPHIER_H
#define STEGANOGRAPHIER_H
#include "file.h"

class Steganographier {
    public:
        virtual File *encode(const string &imgfn, const string &filefn) = 0;
        virtual File *decode(const string &imgfn, const string &filefn = "") = 0;
};

#endif
