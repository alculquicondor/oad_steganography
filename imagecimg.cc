#include "imagecimg.h"

ImageCimg::ImageCimg() {
    img = nullptr;
}

ImageCimg::~ImageCimg() {
    if (img != nullptr)
        delete img;
}

void ImageCimg::save(string filename) const {
    if (img != nullptr)
        img->save_png((filename + ".png").c_str());
}

