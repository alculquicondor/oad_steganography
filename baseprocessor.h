#ifndef BASEPROCESSOR_H
#define BASEPROCESSOR_H
#include "steganographiercimg.h"
#include "file.h"
#include <string>

class BaseProcessor {
    protected:
        Steganographier *steganographier;
    public:
        BaseProcessor() {
            steganographier = new SteganographierCimg;
        }
        virtual File *operator()(const string &imgfn, const string &filefn) = 0;
};

#endif
