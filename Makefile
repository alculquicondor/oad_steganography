# location of the Python header files
 
PYTHON_VERSION = 2.7
PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)
 
# location of the Boost Python include files and library
 
BOOST_INC = /usr/include
BOOST_LIB = /usr/lib
 
# compile mesh classes
TARGET = processor

CC = g++ -std=c++11
DEBUG = #-DDEBUG
 
$(TARGET).so: build/$(TARGET).o build/file.o build/steganographiercimg.o build/imagecimg.o build/encoder.o build/decoder.o
	$(CC) -shared -Wl,--export-dynamic build/decoder.o build/encoder.o build/steganographiercimg.o build/file.o build/imagecimg.o build/$(TARGET).o -L$(BOOST_LIB) -lboost_python -L/usr/lib/python$(PYTHON_VERSION)/config -lpython$(PYTHON_VERSION) -o build/$(TARGET).so -L/usr/X11R6/lib -lm -lpthread -lX11 $(DEBUG)
 
build/$(TARGET).o: $(TARGET).cc
	$(CC) -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -fPIC -c $(TARGET).cc -o build/$(TARGET).o $(DEBUG)

build/steganographiercimg.o: steganographiercimg.cc steganographiercimg.h
	$(CC) -fPIC -c steganographiercimg.cc -o build/steganographiercimg.o $(DEBUG)

build/encoder.o: encoder.cc encoder.h
	$(CC) -fPIC -c encoder.cc -o build/encoder.o $(DEBUG)

build/decoder.o: decoder.cc decoder.h
	$(CC) -fPIC -c decoder.cc -o build/decoder.o $(DEBUG)

build/file.o: file.cc file.h
	$(CC) -fPIC -c file.cc -o build/file.o $(DEBUG)

build/imagecimg.o: imagecimg.cc imagecimg.h
	$(CC) -fPIC -c imagecimg.cc -o build/imagecimg.o $(DEBUG)

clean:
	rm build/*.o build/*.so

