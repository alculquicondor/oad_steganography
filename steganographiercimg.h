#ifndef STEGANOGRAPHIERCIMG_H
#define STEGANOGRAPHIERCIMG_H
#include "steganographier.h"
#include "imagecimg.h"
#include <exception>
#include <cstring>

class SteganographierCimg : public Steganographier {
    public:
        virtual ImageCimg *encode(const string &imgfn, const string &filefn);
        virtual File *decode(const string &imgfn, const string &filefn);
};

#endif
