#include <boost/python.hpp>
#include "encoder.h"
#include "decoder.h"

BOOST_PYTHON_MODULE(processor) {
    using namespace boost::python;
    class_<File>("File")
        .def("save", &File::save)
        .def("size", &File::size)
        .def("get_extension", &File::get_extension);
    class_<Encoder>("Encoder")
        .def("__call__", &Encoder::operator(), return_value_policy<manage_new_object>());
    class_<Decoder>("Decoder")
        .def("__call__", &Decoder::operator(), return_value_policy<manage_new_object>());
}

